/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of android-santi.
 *
 * android-santi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-santi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-santi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.android_santi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gitlab.potatonick.android_santi.client.CouldNotConnectException;
import com.gitlab.potatonick.android_santi.client.SantiClient;
import com.gitlab.potatonick.android_santi.client.SantiException;
import com.gitlab.potatonick.android_santi.client.Server;
import com.gitlab.potatonick.android_santi.client.User;

public class LoginActivity extends AppCompatActivity {

    public static final String HOST_KEY = "HOST";
    public static final String PORT_KEY = "PORT";
    public static final String USERNAME_KEY = "USERNAME";
    public static final String PASSWORD_KEY = "PASSWORD";

    private EditText hostEditText;
    private EditText portEditText;
    private EditText usernameEditText;
    private EditText passwordEditText;
    private ProgressBar loginProgressBar;
    private Button loginButton;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findViews();
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        // Check if we have already saved the server and username in cache
        final String host = sharedPreferences.getString(HOST_KEY, "");
        final int port = sharedPreferences.getInt(PORT_KEY, -1);
        final String user = sharedPreferences.getString(USERNAME_KEY, "");
        final String password = sharedPreferences.getString(PASSWORD_KEY, "");

        if (host.isEmpty() || port < 0 || user.isEmpty() || password.isEmpty()) {
            return;
        }

        // Try to log in from credentials in cache
        hostEditText.setText(host);
        portEditText.setText(Integer.toString(port));
        usernameEditText.setText(user);
        passwordEditText.setText(password);

        new LoginTask().execute();
    }

    private void findViews() {
        this.hostEditText = findViewById(R.id.hostEditText);
        this.portEditText = findViewById(R.id.portEditText);
        this.usernameEditText = findViewById(R.id.userNameEditText);
        this.passwordEditText = findViewById(R.id.passwordEditText);
        this.loginProgressBar = findViewById(R.id.loginProgressBar);
        this.loginButton = findViewById(R.id.loginButton);
    }

    public void logIn(View view) {
        new LoginTask().execute();
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private void openTaskListActivity() {
        final Intent taskListIntent = new Intent();
        taskListIntent.setClass(getApplicationContext(), TaskListActivity.class);
        startActivity(taskListIntent);
        finish();
    }

    private class LoginTask extends AsyncTask<Void, String, Boolean> {

        private Server server;
        private User user;
        private boolean connected = false;

        @Override
        protected void onPreExecute() {
            setWorkingState();
            this.server = createServerFromViews();
            this.user = createUserFromViews();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            SantiClientInstance.createInstance(server, user);
            final SantiClient santiClient = SantiClientInstance.getInstance();
            try {
                boolean result =  santiClient.isAuthenticated();

                // Connection successful
                final SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();
                preferencesEditor.putString(HOST_KEY, server.getHost());
                preferencesEditor.putInt(PORT_KEY, server.getPort());
                preferencesEditor.putString(USERNAME_KEY, user.getUsername());
                preferencesEditor.putString(PASSWORD_KEY, user.getPassword());
                preferencesEditor.apply();
                connected = true;
                return result;
            } catch (CouldNotConnectException e) {
                publishProgress(getResources().getString(R.string.no_server_connection));
                removeSavedPreferences();
                e.printStackTrace();
            } catch (SantiException e) {
                removeSavedPreferences();
                e.printStackTrace();
            }
            return false;
        }

        private void removeSavedPreferences() {
            final SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();
            preferencesEditor.putString(HOST_KEY, "");
            preferencesEditor.putInt(PORT_KEY, -1);
            preferencesEditor.putString(USERNAME_KEY, "");
            preferencesEditor.putString(PASSWORD_KEY, "");
            preferencesEditor.apply();
        }

        @Override
        protected void onPostExecute(Boolean isAuthenticated) {
            if (! connected) {
                setEditableState();
                return;
            }

            if (isAuthenticated) {
                openTaskListActivity();
            } else {
                showToast(getResources().getString(R.string.user_or_password_incorrect));
                setEditableState();
            }
        }

        @Override
        protected void onProgressUpdate(String... values) {
            showToast(values[0]);
        }

        private void setWorkingState() {
            disableAllComponents();
            loginProgressBar.setVisibility(View.VISIBLE);
        }

        private void setEditableState() {
            enableAllComponents();
            loginProgressBar.setVisibility(View.INVISIBLE);
        }

        private void disableAllComponents() {
            hostEditText.setEnabled(false);
            portEditText.setEnabled(false);
            passwordEditText.setEnabled(false);
            usernameEditText.setEnabled(false);
            passwordEditText.setEnabled(false);
            loginButton.setEnabled(false);
        }

        private void enableAllComponents() {
            hostEditText.setEnabled(true);
            portEditText.setEnabled(true);
            passwordEditText.setEnabled(true);
            usernameEditText.setEnabled(true);
            passwordEditText.setEnabled(true);
            loginButton.setEnabled(true);
        }

        private Server createServerFromViews() {
            final String host = hostEditText.getText().toString();
            final int port = Integer.parseInt(portEditText.getText().toString());
            return new Server(host, port);
        }

        private User createUserFromViews() {
            final String username = usernameEditText.getText().toString();
            final String password = passwordEditText.getText().toString();
            return new User(username, password);
        }

    }

}
