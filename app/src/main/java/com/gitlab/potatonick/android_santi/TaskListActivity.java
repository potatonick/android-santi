/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of android-santi.
 *
 * android-santi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-santi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-santi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.android_santi;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gitlab.potatonick.android_santi.client.SantiClient;
import com.gitlab.potatonick.android_santi.client.SantiException;
import com.gitlab.potatonick.android_santi.client.Task;
import com.gitlab.potatonick.android_santi.dialog.DeleteTaskDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

public class TaskListActivity extends AppCompatActivity {

    private static List<Task> tasks;
    private ListView taskListView;
    private TextView emptyTaskList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);
        findViews();

        // Set logic for views
        taskListView.setEmptyView(emptyTaskList);
        taskListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Intent viewTaskIntent = new Intent();
                viewTaskIntent.setClass(getApplicationContext(), DetailTaskActivity.class);
                viewTaskIntent.putExtra(DetailTaskActivity.TASK_KEY_INTENT, position);
                startActivity(viewTaskIntent);
            }
        });
        registerForContextMenu(taskListView);
    }

    @Override
    protected void onResume() {
        // FIXME: Fetching tasks from the server every single time the user gets back to this activity or rotates the screen is a massive and costly task (pun intended)
        super.onResume();
        synchronizeTasks();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.task_list_menu, menu);
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.remove_task_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.remove_task:
                final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                final long taskID = tasks.get(info.position).getId();

                final DeleteTaskDialog deleteDialog = new DeleteTaskDialog();
                deleteDialog.setOnDialogListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        synchronizeTasks();
                    }
                });
                final Bundle bundle = new Bundle();
                bundle.putLong(DeleteTaskDialog.TASK_ID, taskID);

                deleteDialog.setArguments(bundle);
                deleteDialog.show(getSupportFragmentManager(), "delete_task");

                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public static List<Task> getTasks() {
        return Collections.unmodifiableList(tasks);
    }

    private void findViews() {
        this.taskListView = findViewById(R.id.tasklistListView);
        this.emptyTaskList = findViewById(R.id.emptyTextView);
    }

    private void synchronizeTasks() {
        new TaskListTask().execute();
    }

    public void showAddTaskActivity(MenuItem item) {
        final Intent addTaskIntent = new Intent();
        addTaskIntent.setClass(getApplicationContext(), AddTaskActivity.class);
        startActivity(addTaskIntent);
    }

    private void openLoginActivity() {
        final Intent loginIntent = new Intent();
        loginIntent.setClass(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
        finish();
    }

    public void showPreferencesActivity(MenuItem item) {
        final Intent preferencesIntent = new Intent();
        preferencesIntent.setClass(getApplicationContext(), PreferencesActivity.class);
        startActivity(preferencesIntent);
    }

    private class TaskAdapter extends ArrayAdapter<Task> {

        private final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        public TaskAdapter(Context context, List<Task> tasks) {
            super(context, 0, tasks);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                final LayoutInflater layoutInflater = (LayoutInflater) getContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.item_task_list, parent, false);
            }

            final View priorityIndicator = convertView.findViewById(R.id.priorityIndicator);
            final TextView titleTextView = convertView.findViewById(R.id.taskTitle);
            final TextView descriptionTextView = convertView.findViewById(R.id.taskDescription);
            final TextView endDateTextView = convertView.findViewById(R.id.taskEndDate);

            final Task task = getItem(position);
            final Task.Priority priority = task.getPriority();
            final String taskTitle = task.getTitle();
            final String description = task.getDescription();
            final String endDate = dateFormat.format(task.getPlannedEndDate());

            priorityIndicator.setBackgroundColor(convertPriorityToColor(priority));
            titleTextView.setText(taskTitle);
            descriptionTextView.setText(description);
            endDateTextView.setText(endDate);

            return convertView;
        }

        private int convertPriorityToColor(Task.Priority priority) {
            final Resources resources = getResources();
            switch (priority) {
                case LOW:
                    return resources.getColor(R.color.lowPriority);
                case MEDIUM:
                    return resources.getColor(R.color.mediumPriority);
                case HIGH:
                    return resources.getColor(R.color.highPriority);
                case URGENT:
                    return resources.getColor(R.color.urgentPriority);
                default:
                    return resources.getColor(R.color.lowPriority);
            }
        }
    }

    private class TaskListTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                final SantiClient santiClient = SantiClientInstance.getInstance();
                tasks = santiClient.getTasks();
            } catch (SantiException e) {
                openLoginActivity();
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            final TaskAdapter taskAdapter = new TaskAdapter(getApplicationContext(), tasks);
            taskListView.setAdapter(taskAdapter);
        }
    }

}
