/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of android-santi.
 *
 * android-santi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-santi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-santi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.android_santi;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gitlab.potatonick.android_santi.client.Client;
import com.gitlab.potatonick.android_santi.client.Task;
import com.gitlab.potatonick.android_santi.dialog.DeleteTaskDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class DetailTaskActivity extends AppCompatActivity {

    public static final String TASK_KEY_INTENT = "task";
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");

    private Task task;
    private int taskIndex;
    private TextView descriptionTextView;
    private TextView stateTextView;
    private TextView priorityTextView;
    private TextView startDateTextView;
    private TextView endDateTextView;
    private ListView assignedUsersListView;

    private List<Client> assignedClients;
    private ClientAdapter assignedClientAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_task);
        findViews();
        updateScreenFromReceivedTask();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_task_menu, menu);
        return true;
    }

    public void editTask(MenuItem item) {
        final Intent editTaskIntent = new Intent();
        editTaskIntent.setClass(getApplicationContext(), EditTaskActivity.class);
        editTaskIntent.putExtra(EditTaskActivity.TASK_KEY_INTENT, taskIndex);
        startActivity(editTaskIntent);
    }

    public void deleteTask(MenuItem item) {
        final DeleteTaskDialog deleteDialog = new DeleteTaskDialog();
        deleteDialog.setOnDialogListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });

        final Bundle bundle = new Bundle();
        bundle.putLong(DeleteTaskDialog.TASK_ID, task.getId());

        deleteDialog.setArguments(bundle);
        deleteDialog.show(getSupportFragmentManager(), "delete_task");
    }

    private void findViews() {
        this.descriptionTextView = findViewById(R.id.descriptionTextView);
        this.stateTextView = findViewById(R.id.stateTextView);
        this.priorityTextView = findViewById(R.id.priorityTextView);
        this.startDateTextView = findViewById(R.id.startDateTextView);
        this.endDateTextView = findViewById(R.id.endDateTextView);
        this.assignedUsersListView = findViewById(R.id.assignedUsersListView);
    }

    private void updateScreenFromReceivedTask() {
        receiveTask();
        if (task == null) {
            finish();
            return;
        }

        setTitle(task.getTitle());
        descriptionTextView.setText(task.getDescription());

        final String state = transformStateToString(task.getState());
        stateTextView.setText(state);

        final String priority = transformPriorityToString(task.getPriority());
        priorityTextView.setText(priority);

        final String startDate = DATE_FORMAT.format(task.getPlannedStartDate());
        startDateTextView.setText(startDate);

        final String endDate = DATE_FORMAT.format(task.getPlannedEndDate());
        endDateTextView.setText(endDate);

        setListAdapterAndPopulateAssignedClients();
    }

    private void setListAdapterAndPopulateAssignedClients() {
        // Set adapter
        assignedClients = new ArrayList<>();
        assignedClientAdapter = new ClientAdapter(getApplicationContext(), assignedClients);
        assignedUsersListView.setAdapter(assignedClientAdapter);

        // Populate assigned users
        assignedClients.addAll(task.getClients());
    }

    private void receiveTask() {
        final Intent i = getIntent();
        if (i == null) {
            showToast(getResources().getString(R.string.task_does_not_exist));
            finish();
            return;
        }

        taskIndex = i.getIntExtra(TASK_KEY_INTENT, -1);
        if (taskIndex < 0) {
            showToast(getResources().getString(R.string.task_does_not_exist));
            finish();
            return;
        }

        task = TaskListActivity.getTasks().get(taskIndex);
        if (task == null) {
            showToast(getResources().getString(R.string.task_does_not_exist));
            finish();
            return;
        }
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private String transformStateToString(Task.State state) {
        switch(state) {
            case PLANNED:
                return getResources().getString(R.string.planned);
            case IN_PROGRESS:
                return getResources().getString(R.string.in_progress);
            case FINISHED:
                return getResources().getString(R.string.finished);
            default:
                return getResources().getString(R.string.planned);
        }
    }

    private String transformPriorityToString(Task.Priority priority) {
        switch(priority) {
            case LOW:
                return getResources().getString(R.string.low);
            case MEDIUM:
                return getResources().getString(R.string.medium);
            case HIGH:
                return getResources().getString(R.string.high);
            case URGENT:
                return getResources().getString(R.string.urgent);
            default:
                return getResources().getString(R.string.low);
        }
    }

    private class ClientAdapter extends ArrayAdapter<Client> {

        public ClientAdapter(Context context, List<Client> assignedClients) {
            super(context, 0, assignedClients);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                final LayoutInflater layoutInflater = (LayoutInflater) getContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.user_list, parent, false);
            }

            final TextView textView = convertView.findViewById(R.id.username);
            final String clientName = getItem(position).getRealName();
            textView.setText(clientName);

            return convertView;
        }

    }

}
