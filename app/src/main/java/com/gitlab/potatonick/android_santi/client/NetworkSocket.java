/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of android-santi.
 *
 * android-santi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-santi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-santi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.android_santi.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

class NetworkSocket {

    private final Server server;
    private Socket socket;
    private PrintWriter output;
    private BufferedReader input;

    NetworkSocket(Server server) {
        this.server = server;
    }

    synchronized String sendCommandAndGetResponse(String command) {
        try {
            openStream();
            sendCommand(command);
            return getResponse();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        } finally {
            closeStream();
        }
    }

    private void openStream() throws IOException {
        socket = new Socket(server.getHost(), server.getPort());
        output = new PrintWriter(
                new OutputStreamWriter(socket.getOutputStream()),
                true
        );
        input = new BufferedReader(
                new InputStreamReader(socket.getInputStream(), "UTF-8")
        );
    }

    private void sendCommand(String command) {
        output.println(command);
    }

    private String getResponse() throws IOException {
        return input.readLine();
    }

    private void closeStream() {
        try {
            if (socket != null && socket.isConnected()) {
                socket.close();
                socket = null;
                output = null;
                input = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
