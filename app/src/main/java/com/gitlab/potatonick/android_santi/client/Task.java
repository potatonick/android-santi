/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of android-santi.
 *
 * android-santi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-santi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-santi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.android_santi.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Task {

    private long id = 0;
    private String title = "";
    private String description = "";
    private State state = State.PLANNED;
    private Priority priority = Priority.LOW;
    private Date plannedStartDate = new Date();
    private Date plannedEndDate = new Date();
    private List<Client> clients = new ArrayList<>();

    public Task() { }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Task) {
            final Task compare = (Task) obj;
            // We don't care about clients attribute
            return compare.getId() == getId() &&
                   compare.getTitle().equals(getTitle()) &&
                   compare.getDescription().equals(getDescription()) &&
                   compare.getState().equals(getState()) &&
                   compare.getPriority().equals(getPriority()) &&
                   compare.getPlannedStartDate().equals(getPlannedStartDate()) &&
                   compare.getPlannedEndDate().equals(getPlannedEndDate());
        }
        return false;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public Date getPlannedStartDate() {
        return plannedStartDate;
    }

    public void setPlannedStartDate(Date plannedStartDate) {
        this.plannedStartDate = plannedStartDate;
    }

    public Date getPlannedEndDate() {
        return plannedEndDate;
    }

    public void setPlannedEndDate(Date plannedEndDate) {
        this.plannedEndDate = plannedEndDate;
    }

    public void addClient(Client client) {
        this.clients.add(client);
    }

    public List<Client> getClients() {
        return Collections.unmodifiableList(this.clients);
    }

    public void clearClients() {
        this.clients.clear();
    }

    public enum State {
        PLANNED (0),
        IN_PROGRESS (1),
        FINISHED (2);

        private final int value;

        State(int value) {
            this.value = value;
        }

        public int toInteger() {
            return this.value;
        }

        public static State toState(int value) {
            switch(value) {
                case 0:
                    return PLANNED;
                case 1:
                    return IN_PROGRESS;
                case 2:
                    return FINISHED;
                default:
                    return PLANNED;
            }
        }
    }

    public enum Priority {
        LOW (0),
        MEDIUM (1),
        HIGH (2),
        URGENT (3);

        private final int value;

        Priority(int value) {
            this.value = value;
        }

        public int toInteger() {
            return this.value;
        }

        public static Priority toPriority(int value) {
            switch(value) {
                case 0:
                    return LOW;
                case 1:
                    return MEDIUM;
                case 2:
                    return HIGH;
                case 3:
                    return URGENT;
                default:
                    return LOW;
            }
        }
    }

}
