/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of android-santi.
 *
 * android-santi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-santi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-santi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.android_santi.client;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

public class ResponseDecoderTest {

    private static final String STATUS_KEY = "status";
    private static final String ERROR_KEY = "error";
    private static final String DATA_KEY = "data";
    private static final String OK_RESPONSE_VALUE = "ok";
    private static final String ERROR_RESPONSE_VALUE = "error";

    private final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private final Random random = new Random();

    public ResponseDecoderTest() { }

    @Test
    public void shouldHaveNoErrors() {
        final String encodedResponse = createEncodedOkResponse();
        final ResponseDecoder responseDecoder = new ResponseDecoder(encodedResponse);
        assertFalse(responseDecoder.hasErrors());
    }

    @Test
    public void shouldHaveErrors() {
        final String encodedResponse = createEncodedErrorResponse(ResponseDecoder.ErrorType.UNAUTHORIZED);
        final ResponseDecoder responseDecoder = new ResponseDecoder(encodedResponse);
        assertTrue(responseDecoder.hasErrors());
    }

    @Test
    public void shouldGetNoError() {
        final String encodedResponse = createEncodedOkResponse();
        final ResponseDecoder responseDecoder = new ResponseDecoder(encodedResponse);
        assertEquals(ResponseDecoder.ErrorType.NONE, responseDecoder.getErrorType());
    }

    @Test
    public void shouldGetBadFormattingError() {
        final String encodedResponse = "{invalid JSON]";
        final ResponseDecoder responseDecoder = new ResponseDecoder(encodedResponse);
        assertEquals(ResponseDecoder.ErrorType.INCORRECT_RESPONSE_FORMAT, responseDecoder.getErrorType());
    }

    @Test
    public void shouldGetUnauthorizedError() {
        final String encodedResponse = createEncodedErrorResponse(ResponseDecoder.ErrorType.UNAUTHORIZED);
        final ResponseDecoder responseDecoder = new ResponseDecoder(encodedResponse);
        assertEquals(ResponseDecoder.ErrorType.UNAUTHORIZED, responseDecoder.getErrorType());
    }

    @Test
    public void shouldGetServerError() {
        final String encodedResponse = createEncodedErrorResponse(ResponseDecoder.ErrorType.SERVER_ERROR);
        final ResponseDecoder responseDecoder = new ResponseDecoder(encodedResponse);
        assertEquals(ResponseDecoder.ErrorType.SERVER_ERROR, responseDecoder.getErrorType());
    }

    @Test
    public void shouldGetTasks() {
        final List<Task> randomTasks = createRandomTasks();
        final String encodedResponse = createEncodedOkResponseWithRandomTasks(randomTasks);
        final ResponseDecoder responseDecoder = new ResponseDecoder(encodedResponse);
        final List<Task> decodedTasks = responseDecoder.getTasks();

        assertEquals(randomTasks.size(), decodedTasks.size());
        for (int i = 0; i < randomTasks.size(); i++) {
            final Task originalTask = randomTasks.get(i);
            final Task decodedTask = decodedTasks.get(i);
            assertEquals(originalTask, decodedTask);
        }
    }

    @Test
    public void shouldGetEmptyTasks() {
        final String encodedResponse = createEncodedOkResponse();
        final ResponseDecoder responseDecoder = new ResponseDecoder(encodedResponse);
        final List<Task> decodedTasks = responseDecoder.getTasks();
        assertEquals(0, decodedTasks.size());
    }

    private String createEncodedOkResponse() {
        final JSONObject response = new JSONObject();
        response.put(STATUS_KEY, OK_RESPONSE_VALUE);
        return response.toJSONString();
    }

    private String createEncodedErrorResponse(ResponseDecoder.ErrorType errorType) {
        final JSONObject response = new JSONObject();
        response.put(STATUS_KEY, ERROR_RESPONSE_VALUE);
        response.put(ERROR_KEY, errorType.toString());
        return response.toJSONString();
    }

    private String createEncodedOkResponseWithRandomTasks(List<Task> tasks) {
        final JSONObject response = new JSONObject();
        final JSONArray data = new JSONArray();
        for (final Task task : tasks) {
            final JSONObject encodedTask = encodeTask(task);
            data.add(encodedTask);
        }

        response.put(STATUS_KEY, OK_RESPONSE_VALUE);
        response.put(DATA_KEY, data);
        return response.toJSONString();
    }

    private JSONObject encodeTask(Task task) {
        final JSONObject taskObject = new JSONObject();
        taskObject.put("id", task.getId());
        taskObject.put("title", task.getTitle());
        taskObject.put("description", task.getDescription());
        taskObject.put("state", task.getState().toInteger());
        taskObject.put("priority", task.getPriority().toInteger());
        taskObject.put("start-date", dateFormat.format(task.getPlannedStartDate()));
        taskObject.put("end-date", dateFormat.format(task.getPlannedEndDate()));

        final JSONArray users = new JSONArray();
        taskObject.put("users", users);
        for (final Client client : task.getClients()) {
            final JSONObject encodedClient = encodeClient(client);
            users.add(encodedClient);
        }
        return taskObject;
    }

    private JSONObject encodeClient(Client client) {
        final JSONObject encodedClient = new JSONObject();
        encodedClient.put("id", client.getId());
        encodedClient.put("user_name", client.getUsername());
        encodedClient.put("real_name", client.getRealName());
        encodedClient.put("role", client.getRole().toInteger());
        return encodedClient;
    }

    private List<Task> createRandomTasks() {
        final List<Task> tasks = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            final Task task = createRandomTask();
            tasks.add(task);
        }
        return tasks;
    }

    private Task createRandomTask() {
        final Task task = new Task();
        task.setId(random.nextInt());
        task.setTitle(createRandomString());
        task.setDescription(createRandomString());
        task.setState(Task.State.FINISHED);
        task.setPriority(Task.Priority.HIGH);
        task.setPlannedStartDate(getCurrentDateWithoutHours());
        task.setPlannedEndDate(getCurrentDateWithoutHours());
        for (int i = 0; i < 4; i++) {
            final Client randomClient = createRandomClient();
            task.addClient(randomClient);
        }
        return task;
    }

    private Date getCurrentDateWithoutHours() {
        // A little hack to get rid of the hours
        Date ret = null;
        try {
            final Date currentDate = new Date();
            final String formattedDate = dateFormat.format(currentDate);
            return dateFormat.parse(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ret;
    }

    private Client createRandomClient() {
        final Client client = new Client();
        client.setId(random.nextInt());
        client.setUsername(createRandomString());
        client.setRealName(createRandomString());
        client.setRole(Client.Role.USER);
        return client;
    }

    private String createRandomString() {
        return Integer.toString(random.nextInt());
    }

}
