/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of android-santi.
 *
 * android-santi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-santi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-santi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.android_santi;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gitlab.potatonick.android_santi.client.Client;
import com.gitlab.potatonick.android_santi.client.SantiClient;
import com.gitlab.potatonick.android_santi.client.SantiException;
import com.gitlab.potatonick.android_santi.client.Task;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EditTaskActivity extends AppCompatActivity {

    public static final String TASK_KEY_INTENT = "task";

    private TypedArray prioritiesData;
    private TypedArray statesData;

    private EditText titleEditText;
    private EditText descriptionEditText;
    private Spinner prioritySpinner;
    private Spinner stateSpinner;
    private EditText startDateEditText;
    private EditText endDateEditText;
    private Button addUserButton;
    private ListView assignedUsersListView;

    private DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private Task task;
    private List<SelectClient> assignedClients;
    private ClientsAdapter assignedClientsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task);

        getDataForSpinners();
        findViews();
        setEventListenersForSpinners();
        updateScreenFromReceivedTask();
        setAssignedClientsAdapter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_task_mode_menu, menu);
        return true;
    }

    public void saveTask(MenuItem item) {
        createTaskFromForm();
        new ModifyTask().execute();
    }

    private void createTaskFromForm() {
        final String title = titleEditText.getText().toString();
        task.setTitle(title);

        final String description = descriptionEditText.getText().toString();
        task.setDescription(description);

        // State and priority properties are set automatically

        final String startDateFormatted = startDateEditText.getText().toString();
        final Date startDate = parseDateOrGetCurrentDate(startDateFormatted);
        task.setPlannedStartDate(startDate);

        final String endDateFormatted = endDateEditText.getText().toString();
        final Date endDate = parseDateOrGetCurrentDate(endDateFormatted);
        task.setPlannedEndDate(endDate);

        task.clearClients();
        for (final SelectClient selectClient : assignedClients) {
            if (selectClient.isSelected()) {
                task.addClient(selectClient.getClient());
            }
        }
    }

    private Date parseDateOrGetCurrentDate(String formattedDate) {
        try {
            return dateFormat.parse(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    private void getDataForSpinners() {
        this.prioritiesData = getResources().obtainTypedArray(R.array.priorities_data);
        this.statesData = getResources().obtainTypedArray(R.array.states_data);
    }

    private void findViews() {
        this.titleEditText = findViewById(R.id.titleEditText);
        this.descriptionEditText = findViewById(R.id.descriptionEditText);
        this.stateSpinner = findViewById(R.id.stateSpinner);
        this.prioritySpinner = findViewById(R.id.prioritySpinner);
        this.startDateEditText = findViewById(R.id.startDateEditText);
        this.endDateEditText = findViewById(R.id.endDateEditText);
        this.assignedUsersListView = findViewById(R.id.assignedUsersListView);
    }

    private void setEventListenersForSpinners() {
        this.prioritySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String selection = prioritiesData.getString(position);
                switch(selection) {
                    case "low":
                        task.setPriority(Task.Priority.LOW);
                        return;
                    case "medium":
                        task.setPriority(Task.Priority.MEDIUM);
                        return;
                    case "high":
                        task.setPriority(Task.Priority.HIGH);
                        return;
                    case "urgent":
                        task.setPriority(Task.Priority.URGENT);
                        return;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Nothing to do really. This method is defined because it's mandatory to override it
            }
        });
        this.stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String selection = statesData.getString(position);
                switch(selection) {
                    case "planned":
                        task.setState(Task.State.PLANNED);
                        return;
                    case "in progress":
                        task.setState(Task.State.IN_PROGRESS);
                        return;
                    case "finished":
                        task.setState(Task.State.FINISHED);
                        return;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Nothing to do really. This method is defined because it's mandatory to override it
            }
        });
    }

    private void setAssignedClientsAdapter() {
        // Populate assigned clients
        assignedClients = new ArrayList<>();
        assignedClientsAdapter = new ClientsAdapter(getApplicationContext(), assignedClients);
        assignedUsersListView.setAdapter(assignedClientsAdapter);

        new GetAllUsersTask().execute();
    }

    private void updateScreenFromReceivedTask() {
        receiveTask();
        if (task == null) {
            finish();
            return;
        }

        titleEditText.setText(task.getTitle());
        descriptionEditText.setText(task.getDescription());

        stateSpinner.setSelection(task.getState().toInteger());
        prioritySpinner.setSelection(task.getPriority().toInteger());

        final String startDateFormatted = dateFormat.format(task.getPlannedStartDate());
        startDateEditText.setText(startDateFormatted);

        final String endDateFormatted = dateFormat.format(task.getPlannedEndDate());
        endDateEditText.setText(endDateFormatted);
    }

    private void receiveTask() {
        final Intent i = getIntent();
        if (i == null) {
            showToast(getResources().getString(R.string.task_does_not_exist));
            finish();
            return;
        }

        final int taskIndex = i.getIntExtra(TASK_KEY_INTENT, -1);
        if (taskIndex < 0) {
            showToast(getResources().getString(R.string.task_does_not_exist));
            finish();
            return;
        }

        task = TaskListActivity.getTasks().get(taskIndex);
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private class ClientsAdapter extends ArrayAdapter<SelectClient> {

        ClientsAdapter(Context context, List<SelectClient> clients) {
            super(context, 0, clients);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                final LayoutInflater layoutInflater = (LayoutInflater) getContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.item_select_user, parent, false);
            }

            final SelectClient selectClient = getItem(position);
            final Client client = selectClient.getClient();
            final String clientName = client.getRealName();

            final CheckBox userCheckBox = convertView.findViewById(R.id.userCheckBox);
            userCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    selectClient.setSelected(isChecked);
                }
            });

            userCheckBox.setText(clientName);
            return convertView;
        }

    }

    private class ModifyTask extends AsyncTask<Void, Void, Void> {

        private boolean hasErrors = false;

        @Override
        protected Void doInBackground(Void... voids) {
            final SantiClient santiClient = SantiClientInstance.getInstance();
            try {
                santiClient.modifyTask(task);
            } catch (SantiException e) {
                hasErrors = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (hasErrors) {
                showToast(getResources().getString(R.string.could_not_modify_task));
            } else {
                finish();
            }
        }

    }

    private class GetAllUsersTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            final SantiClient santiClient = SantiClientInstance.getInstance();
            try {
                final List<Client> clients = santiClient.getAllClients();

                // Convert Client list to SelectClient list
                for (final Client client : clients) {
                    final SelectClient selectClient = new SelectClient(client, false);
                    assignedClients.add(selectClient);
                }
            } catch (SantiException e) {
                e.printStackTrace();
                showToast(getResources().getString(R.string.could_not_get_clients));
                finish();
            }
            return null;
        }

    }

}
