/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of android-santi.
 *
 * android-santi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-santi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-santi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.android_santi.client;

import com.gitlab.potatonick.android_santi.helper.JSONHelper;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class ResponseDecoder {

    private static final String STATUS_KEY = "status";
    private static final String ERROR_KEY = "error";
    private static final String DATA_KEY = "data";
    private static final String OK_RESPONSE_VALUE = "ok";

    private final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private long clientID = -1;
    private ErrorType errorType = ErrorType.NONE;
    private List<Task> tasks = new ArrayList<>();
    private List<Client> clients = new ArrayList<>();

    ResponseDecoder(String encodedResponse) {
        try {
            if (encodedResponse.isEmpty()) {
                errorType = ErrorType.COULD_NOT_CONNECT;
            } else {
                final JSONParser parser = new JSONParser();
                final JSONObject response = (JSONObject) parser.parse(encodedResponse);
                decodeClientID(response);
                setErrorType(response);
                decodeTasks(response);
                decodeClients(response);
            }
        } catch (ParseException e) {
            errorType = ErrorType.INCORRECT_RESPONSE_FORMAT;
            e.printStackTrace();
        }
    }

    boolean hasErrors() {
        return ! errorType.equals(ErrorType.NONE);
    }

    long getClientID() {
        return clientID;
    }

    ErrorType getErrorType() {
        return errorType;
    }

    List<Task> getTasks() {
        return this.tasks;
    }

    List<Client> getAllClients() {
        return this.clients;
    }

    private void decodeClientID(JSONObject decodedResponse) {
        clientID = JSONHelper.getValue(decodedResponse, "data", Long.class);
    }

    private void setErrorType(JSONObject decodedResponse) {
        final String status = JSONHelper.getValue(decodedResponse, STATUS_KEY, String.class);
        if (status.equals(OK_RESPONSE_VALUE)) {
            errorType = ErrorType.NONE;
        } else {
            final String errorCode = JSONHelper.getValue(decodedResponse, ERROR_KEY, String.class);
            errorType = ErrorType.getErrorType(errorCode);
        }
    }

    private void decodeTasks(JSONObject encodedResponse) {
        final JSONArray encodedTasks = JSONHelper.getValue(encodedResponse, DATA_KEY, JSONArray.class);
        for (int i = 0; i < encodedTasks.size(); i++) {
            final JSONObject encodedTask = JSONHelper.getValue(encodedTasks, i, JSONObject.class);
            final Task decodedTask = decodeTask(encodedTask);
            if (decodedTask != null) {
                tasks.add(decodedTask);
            }
        }
    }

    private Task decodeTask(JSONObject encodedTask) {
        final long id = JSONHelper.getValue(encodedTask, "id", Long.class);
        final String title = JSONHelper.getValue(encodedTask, "title", String.class);
        final String description = JSONHelper.getValue(encodedTask, "description", String.class);
        final int stateNumber = JSONHelper.getValue(encodedTask, "state", Integer.class);
        final Task.State state = Task.State.toState(stateNumber);
        final int priorityNumber = JSONHelper.getValue(encodedTask, "priority", Integer.class);
        final Task.Priority priority = Task.Priority.toPriority(priorityNumber);
        final String startDateString = JSONHelper.getValue(encodedTask, "start_date", String.class);
        final Date startDate = decodeDate(startDateString);
        final String endDateString = JSONHelper.getValue(encodedTask, "end_date", String.class);
        final Date endDate = decodeDate(endDateString);

        final Task task = new Task();
        task.setId(id);
        task.setTitle(title);
        task.setDescription(description);
        task.setState(state);
        task.setPriority(priority);
        task.setPlannedStartDate(startDate);
        task.setPlannedEndDate(endDate);

        final JSONArray users = JSONHelper.getValue(encodedTask, "users", JSONArray.class);
        for (int i = 0; i < users.size(); i++) {
            final JSONObject user = JSONHelper.getValue(users, i, JSONObject.class);
            final Client client = decodeClient(user);
            task.addClient(client);
        }

        return task;
    }

    private void decodeClients(JSONObject encodedResponse) {
        final JSONArray clients = JSONHelper.getValue(encodedResponse, DATA_KEY, JSONArray.class);
        for (int i = 0; i < clients.size(); i++) {
            final JSONObject encodedClient = (JSONObject) clients.get(i);
            final Client decodedClient = decodeClient(encodedClient);
            this.clients.add(decodedClient);
        }
    }

    private Client decodeClient(JSONObject encodedClient) {
        final Client client = new Client();
        final long clientID = JSONHelper.getValue(encodedClient, "id", Long.class);
        final String username = JSONHelper.getValue(encodedClient, "user_name", String.class);
        final String realName = JSONHelper.getValue(encodedClient, "real_name", String.class);
        final int roleInteger = JSONHelper.getValue(encodedClient, "role", Integer.class);
        final Client.Role role = Client.Role.toRole(roleInteger);

        client.setId(clientID);
        client.setUsername(username);
        client.setRealName(realName);
        client.setRole(role);
        return client;
    }

    private Date decodeDate(String encodedDate) {
        try {
            return dateFormat.parse(encodedDate);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public enum ErrorType {
        NONE ("none"),
        COULD_NOT_CONNECT ("could-not-connect"),
        INCORRECT_RESPONSE_FORMAT ("incorrect-response-format"),
        UNAUTHORIZED ("unauthorized"),
        SERVER_ERROR ("server-error"),
        UNKNOWN_COMMAND ("unknown-command");

        private final String value;

        ErrorType(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        public static ErrorType getErrorType(String value) {
            switch(value) {
                case "none":
                    return NONE;
                case "could-not-connect":
                    return COULD_NOT_CONNECT;
                case "incorrect-response-format":
                    return INCORRECT_RESPONSE_FORMAT;
                case "unauthorized":
                    return UNAUTHORIZED;
                case "server-error":
                    return SERVER_ERROR;
                case "unknown-command":
                    return UNKNOWN_COMMAND;
                default:
                    return SERVER_ERROR;
            }
        }
    }

}
