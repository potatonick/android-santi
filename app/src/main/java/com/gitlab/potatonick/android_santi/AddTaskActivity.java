/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of android-santi.
 *
 * android-santi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-santi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-santi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.android_santi;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gitlab.potatonick.android_santi.client.Client;
import com.gitlab.potatonick.android_santi.client.SantiClient;
import com.gitlab.potatonick.android_santi.client.SantiException;
import com.gitlab.potatonick.android_santi.client.Task;
import com.gitlab.potatonick.android_santi.client.User;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddTaskActivity extends AppCompatActivity {

    private TypedArray prioritiesData;
    private TypedArray statesData;

    private EditText titleEditText;
    private EditText descriptionEditText;
    private Spinner prioritySpinner;
    private Spinner stateSpinner;
    private EditText startDateEditText;
    private EditText endDateEditText;
    private ListView assignedUsers;

    private final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private Task task;
    private ArrayAdapter<SelectClient> clientsAdapter;
    private List<SelectClient> clients;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        this.task = new Task();
        this.clients = new ArrayList<>();

        showBackButtonInHeaderBar();
        getDataForSpinners();
        findViews();
        setEventListenersForSpinners();
        registerViewsAllowedToOpenContextualMenus();
        setListAdapter();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.assigned_user_menu, menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_task_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.remove_assigned_user:
                // TODO: Remove this dead code
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public void addNewTask(MenuItem item) {
        createTaskFromForm();
        new AddTask().execute();
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private void createTaskFromForm() {
        final String title = titleEditText.getText().toString();
        task.setTitle(title);

        final String description = descriptionEditText.getText().toString();
        task.setDescription(description);

        // State and priority properties are set automatically

        final String startDateFormatted = startDateEditText.getText().toString();
        final Date startDate = parseDateOrGetCurrentDate(startDateFormatted);
        task.setPlannedStartDate(startDate);

        final String endDateFormatted = endDateEditText.getText().toString();
        final Date endDate = parseDateOrGetCurrentDate(endDateFormatted);
        task.setPlannedEndDate(endDate);

        for (final SelectClient selectClient : clients) {
            if (selectClient.isSelected()) {
                final Client client = selectClient.getClient();
                task.addClient(client);
            }
        }
    }

    private Date parseDateOrGetCurrentDate(String formattedDate) {
        try {
            return dateFormat.parse(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    private void getDataForSpinners() {
        this.prioritiesData = getResources().obtainTypedArray(R.array.priorities_data);
        this.statesData = getResources().obtainTypedArray(R.array.states_data);
    }

    private void showBackButtonInHeaderBar() {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_close);
        }
    }

    private void findViews() {
        this.titleEditText = findViewById(R.id.titleEditText);
        this.descriptionEditText = findViewById(R.id.descriptionEditText);
        this.prioritySpinner = findViewById(R.id.prioritySpinner);
        this.stateSpinner = findViewById(R.id.stateSpinner);
        this.startDateEditText = findViewById(R.id.startDateEditText);
        this.endDateEditText = findViewById(R.id.endDateEditText);
        this.assignedUsers = findViewById(R.id.assignedUsersListView);
    }

    private void setEventListenersForSpinners() {
        this.prioritySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String selection = prioritiesData.getString(position);
                switch(selection) {
                    case "low":
                        task.setPriority(Task.Priority.LOW);
                        return;
                    case "medium":
                        task.setPriority(Task.Priority.MEDIUM);
                        return;
                    case "high":
                        task.setPriority(Task.Priority.HIGH);
                        return;
                    case "urgent":
                        task.setPriority(Task.Priority.URGENT);
                        return;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Nothing to do really. This method is defined because it's mandatory to override it
            }
        });
        this.stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String selection = statesData.getString(position);
                switch(selection) {
                    case "planned":
                        task.setState(Task.State.PLANNED);
                        return;
                    case "in progress":
                        task.setState(Task.State.IN_PROGRESS);
                        return;
                    case "finished":
                        task.setState(Task.State.FINISHED);
                        return;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Nothing to do really. This method is defined because it's mandatory to override it
            }
        });
    }

    private void registerViewsAllowedToOpenContextualMenus() {
        registerForContextMenu(assignedUsers);
    }

    private void setListAdapter() {
        final SantiClient santiClient = SantiClientInstance.getInstance();
        final User user = santiClient.getUser();

        new GetAllUsersTask().execute();

        clientsAdapter = new ClientsAdapter(getApplicationContext(), clients);
        assignedUsers.setAdapter(clientsAdapter);
    }

    private class ClientsAdapter extends ArrayAdapter<SelectClient> {

        ClientsAdapter(Context context, List<SelectClient> clients) {
            super(context, 0, clients);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                final LayoutInflater layoutInflater = (LayoutInflater) getContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.item_select_user, parent, false);
            }

            final SelectClient selectClient = getItem(position);
            final Client client = selectClient.getClient();
            final String clientName = client.getRealName();

            final CheckBox userCheckBox = convertView.findViewById(R.id.userCheckBox);
            userCheckBox.setText(clientName);
            userCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    selectClient.setSelected(isChecked);
                }
            });

            return convertView;
        }
    }

    private class AddTask extends AsyncTask<Void, Void, Void> {

        private boolean hasErrors = false;

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                final SantiClient santiClient = SantiClientInstance.getInstance();
                santiClient.createTask(task);
            } catch (SantiException e) {
                hasErrors = true;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (hasErrors) {
                showToast(getResources().getString(R.string.save_task_error));
            }

            finish();
        }

    }

    private class GetAllUsersTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            final SantiClient santiClient = SantiClientInstance.getInstance();
            try {
                final List<Client> allClients = santiClient.getAllClients();

                // Convert Client list to SelectClient list
                for (final Client client : allClients) {
                    final SelectClient selectClient = new SelectClient(client, false);
                    clients.add(selectClient);
                }
            } catch (SantiException e) {
                e.printStackTrace();
                showToast(getResources().getString(R.string.could_not_get_clients));
                finish();
            }
            return null;
        }

    }

}
