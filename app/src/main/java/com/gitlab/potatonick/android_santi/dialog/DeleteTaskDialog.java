/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of android-santi.
 *
 * android-santi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-santi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-santi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.android_santi.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

import com.gitlab.potatonick.android_santi.R;
import com.gitlab.potatonick.android_santi.SantiClientInstance;
import com.gitlab.potatonick.android_santi.client.SantiClient;
import com.gitlab.potatonick.android_santi.client.SantiException;

public class DeleteTaskDialog extends DialogFragment {

    public static final String TASK_ID = "ID";

    private long taskID;
    private DialogInterface.OnDismissListener listener;

    public void setOnDialogListener(DialogInterface.OnDismissListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        taskID = getArguments().getLong(TASK_ID);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String title = getResources().getString(R.string.delete_task_title);
        final String message = getResources().getString(R.string.delete_task_message);
        final String positiveAction = getResources().getString(R.string.delete);
        final String negativeAction = getResources().getString(R.string.no);

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity())
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveAction, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    new DeleteTask().execute();
                }
            })
            .setNegativeButton(negativeAction, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dismiss();
                }
            });
        return dialog.create();
    }

    private class DeleteTask extends AsyncTask<Void, Void, Void> {

        private boolean hasErrors = false;

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                final SantiClient santiClient = SantiClientInstance.getInstance();
                santiClient.deleteTaskById(taskID);
                listener.onDismiss(getDialog());
            } catch (SantiException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (hasErrors) {
                Toast.makeText(
                        getActivity().getApplicationContext(),
                        getResources().getString(R.string.could_not_delete_task),
                        Toast.LENGTH_LONG
                );
            }

            dismiss();
        }

    }

}
