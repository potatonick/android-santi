/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of android-santi.
 *
 * android-santi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-santi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-santi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.android_santi.client;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SantiClient {

    // FIXME: Exception should be thrown from ResponseDecoder directly

    private static final String CHECK_CREDENTIALS_COMMAND = "check-credentials";
    private static final String GET_TASKS_COMMAND = "get-tasks";
    private static final String CREATE_TASK_COMMAND = "create-task";
    private static final String REMOVE_TASK_COMMAND = "remove-task";
    private static final String UPDATE_TASK_COMMAND = "update-task";

    private final User user;
    private final Server server;
    private final RequestEncoder requestEncoder;
    private final NetworkSocket networkSocket;

    public SantiClient(Server server, User user) {
        this.user = user;
        this.server = server;
        this.requestEncoder = new RequestEncoder(user);
        this.networkSocket = new NetworkSocket(server);
    }

    public User getUser() {
        return this.user;
    }

    public Server getServer() {
        return this.server;
    }

    public boolean isAuthenticated() throws SantiException {
        final String encodedRequest = requestEncoder.encodeRequest(CHECK_CREDENTIALS_COMMAND);
        final String encodedResponse = networkSocket.sendCommandAndGetResponse(encodedRequest);
        if (encodedResponse.isEmpty()) {
            throw new CouldNotConnectException("Could not connect to server");
        }

        final ResponseDecoder responseDecoder = new ResponseDecoder(encodedResponse);
        user.setID(responseDecoder.getClientID());
        return ! responseDecoder.hasErrors();
    }

    public List<Task> getTasks() throws SantiException {
        final String encodedRequest = requestEncoder.encodeRequest(GET_TASKS_COMMAND);
        final String encodedResponse = networkSocket.sendCommandAndGetResponse(encodedRequest);
        if (encodedResponse.isEmpty()) {
            throw new CouldNotConnectException("Could not connect to server");
        }

        final ResponseDecoder responseDecoder = new ResponseDecoder(encodedResponse);
        throwErrorIfAny(responseDecoder.getErrorType());
        return responseDecoder.getTasks();
    }

    public void createTask(Task task) throws SantiException {
        final String encodedRequest = requestEncoder.encodeRequestWithTask(CREATE_TASK_COMMAND, task);
        final String encodedResponse = networkSocket.sendCommandAndGetResponse(encodedRequest);
        if (encodedResponse.isEmpty()) {
            throw new CouldNotConnectException("Could not connect to server");
        }

        final ResponseDecoder responseDecoder = new ResponseDecoder(encodedResponse);
        throwErrorIfAny(responseDecoder.getErrorType());
    }

    public void modifyTask(Task task) throws SantiException {
        final String encodedRequest = requestEncoder.encodeRequestWithTask(UPDATE_TASK_COMMAND, task);
        final String encodedResponse = networkSocket.sendCommandAndGetResponse(encodedRequest);
        if (encodedResponse.isEmpty()) {
            throw new CouldNotConnectException("Could not connect to server");
        }

        final ResponseDecoder responseDecoder = new ResponseDecoder(encodedResponse);
        throwErrorIfAny(responseDecoder.getErrorType());
    }

    public void deleteTaskById(long taskID) throws SantiException {
        final String encodedRequest = requestEncoder.encodeRequestWithID(REMOVE_TASK_COMMAND, taskID);
        final String encodedResponse = networkSocket.sendCommandAndGetResponse(encodedRequest);
        if (encodedRequest.isEmpty()) {
            throw new CouldNotConnectException("Could not connect to server");
        }

        final ResponseDecoder responseDecoder = new ResponseDecoder(encodedResponse);
        throwErrorIfAny(responseDecoder.getErrorType());
    }

    public List<Client> getAllClients() throws SantiException {
        final String encodedRequest = requestEncoder.encodeRequest("get-users");
        final String encodeResponse = networkSocket.sendCommandAndGetResponse(encodedRequest);
        if (encodedRequest.isEmpty()) {
            throw new CouldNotConnectException("Could not connect to server");
        }

        final ResponseDecoder responseDecoder = new ResponseDecoder(encodeResponse);

        // Workaround for removing repeated users (it's a bug from the server)
        final Set<Client> allClients = new HashSet<>();
        allClients.addAll(responseDecoder.getAllClients());

        final List<Client> clients = new ArrayList<>();
        clients.addAll(allClients);
        return clients;
    }

    private void throwErrorIfAny(ResponseDecoder.ErrorType errorType) throws SantiException {
        switch(errorType) {
            case COULD_NOT_CONNECT:
                throw new CouldNotConnectException("Could not connect to server");
            case UNAUTHORIZED:
                throw new UnauthorizedException("User or password incorrect");
            case SERVER_ERROR:
                throw new ServerErrorException("Server could not process incoming request");
            case UNKNOWN_COMMAND:
                throw new UnknownCommandException("Server cannot process unknown command");
        }
    }

}
