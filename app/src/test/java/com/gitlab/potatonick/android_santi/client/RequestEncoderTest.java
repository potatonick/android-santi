/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of android-santi.
 *
 * android-santi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-santi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-santi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.android_santi.client;

import com.gitlab.potatonick.android_santi.helper.JSONHelper;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class RequestEncoderTest {

    private static final String USER_KEY = "user";
    private static final String PASSWORD_KEY = "password";
    private static final String COMMAND_KEY = "command";
    private static final String DATA_KEY = "data";

    private final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private final JSONParser jsonParser = new JSONParser();
    private final User user = new User("miguel", "1234");
    private final RequestEncoder requestEncoder = new RequestEncoder(user);

    public RequestEncoderTest() { }

    @Test
    public void shouldEncodeCheckCredentialsCommand() throws ParseException {
        final String command = "check-credentials";
        final String request = requestEncoder.encodeRequest(command);

        final JSONObject requestObject = (JSONObject) jsonParser.parse(request);
        final String encodedUser = JSONHelper.getValue(requestObject, USER_KEY, String.class);
        final String encodedPassword = JSONHelper.getValue(requestObject, PASSWORD_KEY, String.class);
        final String encodedCommand = JSONHelper.getValue(requestObject, COMMAND_KEY, String.class);

        assertEquals(user.getUsername(), encodedUser);
        assertEquals(user.getPassword(), encodedPassword);
        assertEquals(command, encodedCommand);
    }

    @Test
    public void shouldEncodeRemoveTask() throws ParseException {
        final long taskID = 1;
        final String command = "remove-task";
        final String request = requestEncoder.encodeRequestWithID(command, taskID);

        final JSONObject requestObject = (JSONObject) jsonParser.parse(request);
        final String encodedUser = JSONHelper.getValue(requestObject, USER_KEY, String.class);
        final String encodedPassword = JSONHelper.getValue(requestObject, PASSWORD_KEY, String.class);
        final String encodedCommand = JSONHelper.getValue(requestObject, COMMAND_KEY, String.class);
        final long encodedID = JSONHelper.getValue(requestObject, DATA_KEY, Long.class);

        assertEquals(user.getUsername(), encodedUser);
        assertEquals(user.getPassword(), encodedPassword);
        assertEquals(command, encodedCommand);
        assertEquals(taskID, encodedID);
    }

    @Test
    public void shouldEncodeCreateTask() throws ParseException, java.text.ParseException {
        final Task task = createTask();
        final String command = "create-task";
        final String request = requestEncoder.encodeRequestWithTask(command, task);

        final JSONObject requestObject = (JSONObject) jsonParser.parse(request);
        final String encodedUser = JSONHelper.getValue(requestObject, USER_KEY, String.class);
        final String encodedPassword = JSONHelper.getValue(requestObject, PASSWORD_KEY, String.class);
        final String encodedCommand = JSONHelper.getValue(requestObject, COMMAND_KEY, String.class);
        final JSONObject encodedTaskObject = JSONHelper.getValue(requestObject, DATA_KEY, JSONObject.class);
        final Task encodedTask = decodeTask(encodedTaskObject);

        assertEquals(user.getUsername(), encodedUser);
        assertEquals(user.getPassword(), encodedPassword);
        assertEquals(command, encodedCommand);
        assertEquals(task, encodedTask);
    }

    private Task createTask() throws java.text.ParseException {
        final Task task = new Task();
        task.setId(1);
        task.setTitle("A random task");
        task.setDescription("For testing purposes");
        task.setState(Task.State.IN_PROGRESS);
        task.setPriority(Task.Priority.URGENT);
        task.setPlannedStartDate(dateFormat.parse("20-04-2018"));
        task.setPlannedEndDate(dateFormat.parse("30-06-2019"));
        task.addClient(createClient());
        return task;
    }

    private Client createClient() {
        final Client client = new Client();
        client.setId(6);
        client.setUsername("pepito_perez");
        client.setRealName("Pepito Pérez");
        client.setRole(Client.Role.ADMIN);
        return client;
    }

    private Task decodeTask(JSONObject encodedTask) throws java.text.ParseException {
        final long id = JSONHelper.getValue(encodedTask, "id", Long.class);
        final String title = JSONHelper.getValue(encodedTask, "title", String.class);
        final String description = JSONHelper.getValue(encodedTask, "description", String.class);
        final int stateInteger = JSONHelper.getValue(encodedTask, "state", Integer.class);
        final Task.State state = Task.State.toState(stateInteger);
        final int priorityInteger = JSONHelper.getValue(encodedTask, "priority", Integer.class);
        final Task.Priority priority = Task.Priority.toPriority(priorityInteger);
        final String startDateString = JSONHelper.getValue(encodedTask, "start-date", String.class);
        final Date startDate = dateFormat.parse(startDateString);
        final String endDateString = JSONHelper.getValue(encodedTask, "end-date", String.class);
        final Date endDate = dateFormat.parse(endDateString);
        final JSONArray clients = JSONHelper.getValue(encodedTask, "users", JSONArray.class);

        final Task decodedTask = new Task();
        decodedTask.setId(id);
        decodedTask.setTitle(title);
        decodedTask.setDescription(description);
        decodedTask.setState(state);
        decodedTask.setPriority(priority);
        decodedTask.setPlannedStartDate(startDate);
        decodedTask.setPlannedEndDate(endDate);
        for (int i = 0; i < clients.size(); i++) {
            final long clientID = JSONHelper.getValue(clients, i, Long.class);
            final Client client = new Client();
            client.setId(clientID);
            decodedTask.addClient(client);
        }
        return decodedTask;
    }

}
