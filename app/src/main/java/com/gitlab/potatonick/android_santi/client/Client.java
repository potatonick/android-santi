/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of android-santi.
 *
 * android-santi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-santi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-santi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.android_santi.client;

import java.util.Objects;

public class Client {

    private long id = 0;
    private String username = "";
    private String realName = "";
    private Role role = Role.USER;

    public Client() { }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Client) {
            final Client compare = (Client) obj;
            return compare.getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (int) id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public enum Role {
        USER (0),
        ADMIN (1);

        private final int value;

        Role(int value) {
            this.value = value;
        }

        public int toInteger() {
            return this.value;
        }

        public static Role toRole(int value) {
            switch(value) {
                case 0:
                    return USER;
                case 1:
                    return ADMIN;
                default:
                    return USER;
            }
        }
    }

}
