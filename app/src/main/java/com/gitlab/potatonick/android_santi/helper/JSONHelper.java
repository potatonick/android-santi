/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of android-santi.
 *
 * android-santi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-santi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-santi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.android_santi.helper;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;

public final class JSONHelper {

    private static final Map<Class<?>, Object> FALLBACK_VALUES;

    static {
        FALLBACK_VALUES = new HashMap<>();
        FALLBACK_VALUES.put(String.class, "");
        FALLBACK_VALUES.put(Integer.class, 0);
        FALLBACK_VALUES.put(Long.class, 0L);
        FALLBACK_VALUES.put(Float.class, 0.0f);
        FALLBACK_VALUES.put(Double.class, 0.0);
        FALLBACK_VALUES.put(Boolean.class, false);
        FALLBACK_VALUES.put(JSONArray.class, new JSONArray());
        FALLBACK_VALUES.put(JSONObject.class, new JSONObject());
    }

    private JSONHelper() {}

    // Returns either the original value or empty (not null)
    // if the key does not exist. If the data type is not
    // supported it returns null
    public static <T> T getValue(JSONObject object, String key, Class<T> type) {
        T value = (T) object.get(key);
        if (value == null) {
            return getFallbackValueIfSupportedOrNull(type);
        } else if (value.getClass().isAssignableFrom(type)) {
            return value;
        } else {
            return tryConvertValueToType(value, type);
        }
    }

    // Returns either the original value or empty (not null)
    // if the index does not exist. If the data type is not
    // supported it returns null
    public static <T> T getValue(JSONArray array, int index, Class<T> type) {
        if (index > array.size()) {
            return getFallbackValueIfSupportedOrNull(type);
        }

        T value = (T) array.get(index);
        if (value == null) {
            return getFallbackValueIfSupportedOrNull(type);
        } else if (value.getClass().isAssignableFrom(type)) {
            return value;
        } else {
            return tryConvertValueToType(value, type);
        }
    }

    private static <T> T getFallbackValueIfSupportedOrNull(Class<T> type) {
        return (T) FALLBACK_VALUES.get(type);
    }

    private static <T> T tryConvertValueToType(T value, Class<T> type) {
        try {
            Number valueNumber = (Number) value;
            if (type.isAssignableFrom(String.class)) {
                return (T) value.toString();
            } else if (type.isAssignableFrom(Integer.class)) {
                return (T) new Integer(valueNumber.intValue());
            } else if (type.isAssignableFrom(Long.class)) {
                return (T) new Long(valueNumber.longValue());
            } else if (type.isAssignableFrom(Float.class)) {
                return (T) new Float(valueNumber.floatValue());
            } else if (type.isAssignableFrom(Double.class)) {
                return (T) new Double(valueNumber.doubleValue());
            }
        } catch (ClassCastException ignored) { }

        return getFallbackValueIfSupportedOrNull(type);
    }

}
