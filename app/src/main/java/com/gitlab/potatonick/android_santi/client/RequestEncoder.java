/*
 * Copyright 2018 Miguel Gutiérrez
 *
 * This file is part of android-santi.
 *
 * android-santi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * android-santi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with android-santi.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.potatonick.android_santi.client;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

class RequestEncoder {

    private static final String USER_KEY = "user";
    private static final String PASSWORD_KEY = "password";
    private static final String COMMAND_KEY = "command";
    private static final String DATA_KEY = "data";

    private final DateFormat dateFormat;
    private final User user;

    RequestEncoder(User user) {
        this.dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        this.user = user;
    }

    String encodeRequest(String command) {
        final JSONObject request = createSimpleRequest(command);
        return request.toJSONString();
    }

    String encodeRequestWithID(String command, long id) {
        final JSONObject request = createSimpleRequest(command);
        request.put(DATA_KEY, id);
        return request.toJSONString();
    }

    String encodeRequestWithTask(String command, Task task) {
        final JSONObject request = createSimpleRequest(command);
        request.put(DATA_KEY, encodeTask(task));
        return request.toJSONString();
    }

    private JSONObject createSimpleRequest(String command) {
        final JSONObject request = new JSONObject();
        request.put(USER_KEY, user.getUsername());
        request.put(PASSWORD_KEY, user.getPassword());
        request.put(COMMAND_KEY, command);
        return request;
    }

    private JSONObject encodeTask(Task task) {
        final JSONObject encodedTask = new JSONObject();
        encodedTask.put("id", task.getId());
        encodedTask.put("title", task.getTitle());
        encodedTask.put("description", task.getDescription());
        encodedTask.put("state", task.getState().toInteger());
        encodedTask.put("priority", task.getPriority().toInteger());
        encodedTask.put("start-date", dateFormat.format(task.getPlannedStartDate()));
        encodedTask.put("end-date", dateFormat.format(task.getPlannedEndDate()));

        final JSONArray users = new JSONArray();
        for (final Client client : task.getClients()) {
            users.add(client.getId());
        }
        encodedTask.put("users", users);

        return encodedTask;
    }

}
